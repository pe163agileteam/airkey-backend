from src import create_app

app = create_app()
app.run(app.config['HOST'], app.config['PORT'])
