from flask import Flask

from config import Config
from src.blueprints import blueprints


def register_blueprints(app):
    for blueprint in blueprints:
        app.register_blueprint(blueprint)


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)

    register_blueprints(app)

    return app
