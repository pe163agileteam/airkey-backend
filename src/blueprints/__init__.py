from .api import api
from .users import users
from .keys import keys
from .locks import locks

blueprints = [api, users, keys, locks]
