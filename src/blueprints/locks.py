from flask import Blueprint

locks = Blueprint('locks', __name__, url_prefix='/locks')


@locks.route('/')
def index():
    return 'locks'
