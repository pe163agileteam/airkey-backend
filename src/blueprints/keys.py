from flask import Blueprint

keys = Blueprint('keys', __name__, url_prefix='/keys')


@keys.route('/')
def index():
    return 'keys'
