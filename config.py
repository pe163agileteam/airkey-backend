import os


class Config:
    DATABASE_URL = os.environ.get('DATABASE_URL')
    HOST = 'localhost'
    PORT = 8000
